package com.example.menu;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class HW_Card_Activity extends AppCompatActivity {

    private FloatingActionButton mFbAddSubject;
    private LinearLayout mLlContainer;
    private AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hw__card_);
        mFbAddSubject=findViewById(R.id.fab_add_subject);
        mLlContainer=findViewById(R.id.linear_container);
        mFbAddSubject.setOnClickListener(v -> {
            PopupMenu popupMenu = new PopupMenu(HW_Card_Activity.this,mFbAddSubject);
            popupMenu.getMenuInflater().inflate(R.menu.subject_menu,popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    int menuId=item.getItemId();
                    switch (menuId)
                    {
                        case R.id.menu_add:
                            AlertDialog.Builder builder = new AlertDialog.Builder(HW_Card_Activity.this);
                            String[] subs={"IOS","Android","Laravel","Web Design","Java","CSharp"};

                            builder.setItems(subs, (dialog, which) -> {
                                /* create cardview*/
                                CardView cardView = new CardView(HW_Card_Activity.this);
                                cardView.setContentPadding(25,25,25,25);
                                cardView.setUseCompatPadding(true);
                                cardView.setCardElevation(2);
                                cardView.setRadius(25);

                                LinearLayout.LayoutParams cardlayout = new LinearLayout.LayoutParams(
                                        LinearLayout.LayoutParams.MATCH_PARENT,
                                        LinearLayout.LayoutParams.MATCH_PARENT
                                );
                                cardView.setLayoutParams(cardlayout);
                                /* create textview*/
                                TextView textView = new TextView(HW_Card_Activity.this);
                                textView.setText(subs[which]);
                                textView.setTextSize(22);
                                textView.setTypeface(Typeface.DEFAULT_BOLD);

                                CardView.LayoutParams textLayout = new CardView.LayoutParams(
                                        CardView.LayoutParams.MATCH_PARENT,
                                        CardView.LayoutParams.WRAP_CONTENT
                                );
                                textLayout.setMargins(25,25,25,25);
                                textView.setLayoutParams(textLayout);
                                /*Add textview to cardview*/
                                cardView.addView(textView);
                                /*Add cardview to Linear*/
                                mLlContainer.addView(cardView);
                            });builder.create().show();
                            break;
                        case R.id.menu_remove_all:
                            builder=new AlertDialog.Builder(HW_Card_Activity.this);
                            builder.setTitle("Confirmation")
                                .setMessage("Are you sure to remove all?")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        mLlContainer.removeAllViews();
                                    }
                                })
                                .setNegativeButton("Cacel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                }).create().show();
                            break;
                    }
                    return false;
                }
            });
            popupMenu.show();
        });

    }
}
