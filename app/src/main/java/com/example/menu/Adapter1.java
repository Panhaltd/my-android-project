package com.example.menu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.menu.custom.Custom_Adapter;
import com.example.menu.custom.Hw_Custom_Adapter;
import com.example.menu.custom.NewActivityAdapter;

public class Adapter1 extends AppCompatActivity {
    static final String ADAPTER_DIALOG = "Custom Adapter";

//    String[] players = {"Messi","Ronaldo","Neymar","Silva","Marcelo","De Gea"};
//    String[] step= new String[50];

    int[] profile = {R.drawable.pp1,R.drawable.pp6,R.drawable.pp3,R.drawable.pp4,R.drawable.pp5,R.drawable.pp7};
    String[] name = {"William Jame","Johnny English","Annie Marie","Harry Hart","Peter Parker","Sam Smith"};
    String[] job = {"Actor","Footballer","Singer","Web Developer","Doctor","Teacher"};
    String[] sex = {"Male","Male","Female","Male","Male","Male"};
    String[] company = {"Kong Chak","PPCFC","Hang Meas","KSHRD","Calmet Hospital","Beltei School"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapter1);
        final ListView mLvPlayer = findViewById(R.id.list_adapter);

        Custom_Adapter customadapter = new Custom_Adapter(this,profile,name,job,sex,company);
        mLvPlayer.setAdapter(customadapter);
        mLvPlayer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Object name = customadapter.getItem(position);
                Object photolink = customadapter.getPhoto(position);
                Object job = customadapter.getJob((position));
                Object sex = customadapter.getSex((position));
                Object company = customadapter.getCompany((position));

                Intent intent = new Intent(Adapter1.this, NewActivityAdapter.class);
                intent.putExtra("Name",name.toString());
                intent.putExtra("Picture",(Integer)photolink);
                intent.putExtra("Job",job.toString());
                intent.putExtra("Sex",sex.toString());
                intent.putExtra("Company",company.toString());
                startActivity(intent);



//                Object name = customadapter.getItem(position);
//                Object photolink = customadapter.getPhoto(position);
//                Object job = customadapter.getJob((position));
////                Toast.makeText(Adapter1.this,name.toString()+photolink.toString()+job.toString(),Toast.LENGTH_LONG).show();
//                AlertDialog.Builder builder = new AlertDialog.Builder(Adapter1.this);
//                View view1 = getLayoutInflater().inflate(R.layout.custom_dialog_adapter,null);
//                builder.setView(view1);
//                final TextView mTvName= view1.findViewById(R.id.tv_name);
////                TextView mTvName = (TextView)view1.findViewById(R.id.tv_name);
//                final TextView mTvJob= view1.findViewById(R.id.tv_job);
//                final ImageView mImage = view1.findViewById(R.id.imageView);
//
//                mTvName.setText(name.toString());
//                mTvJob.setText(job.toString());
//                mImage.setImageResource((Integer)photolink);
////                builder.setMessage("Hi");
//                builder.create().show();
            }
        });


//        for(int i=0;i<50;i++)
//        { step[i]="Step-0"+(i+1);
//        }
//
//        ArrayAdapter<String> playerAdapter = new ArrayAdapter<>(Adapter1.this,
//                                        R.layout.player_textview,R.id.text_player,step);
//
//        mLvPlayer.setAdapter(playerAdapter);
    }
}
