package com.example.menu.custom;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.example.menu.R;

import java.util.jar.Attributes;

import de.hdodenhof.circleimageview.CircleImageView;

public class NewActivityAdapter extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_adapter);
        final CircleImageView mCIprofile= findViewById(R.id.cicrle_profile);
        final TextView mTvProfileName = findViewById(R.id.tv_profile_name);
        final Button mBtback =findViewById(R.id.bt_back);
        final TextView mTvSex = findViewById(R.id.text_sex);
        final TextView mTvCompany = findViewById(R.id.text_Company);
        final TextView mTvJob = findViewById(R.id.text_pf_job);

        Bundle extra = getIntent().getExtras();

        String profilename = extra.getString("Name");
        Integer profilepic = extra.getInt("Picture");
        String profilesex = extra.getString("Sex");
        String profilecompany = extra.getString("Company");
        String profilejob = extra.getString("Job");
        mCIprofile.setImageResource(profilepic);
        mTvProfileName.setText(profilename);
        mTvJob.setText(profilejob);
        mTvCompany.setText(profilecompany);
        mTvSex.setText(profilesex);


        mBtback.setOnClickListener(v -> {
            Intent backIntent = getIntent();
            setResult(RESULT_OK,backIntent);
            finish();
        });

    }
}
