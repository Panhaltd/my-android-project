package com.example.menu.custom;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.menu.R;
import com.github.javafaker.Faker;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class NewActivityGridView extends AppCompatActivity {

    private TextView mTvPostNum;
    private TextView mTvFollowerNum;
    private TextView mTvFollowingNum;
    private TextView mTvProfileName;
    private TextView mTvLinkEmail;
    private CircleImageView mCIProfileImage;
    private Button mBtFollow;
    private String[] newImage=new String[30];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_grid_view);
        final GridView mMyGridView = findViewById(R.id.new_grid);
        initView();

        Bundle extra = getIntent().getExtras();
        String postnum = extra.getString("NumPost");
        String numFollowing = extra.getString("NumFollowing");
        String numFollower = extra.getString("NumFollower");
        String name = extra.getString("Name");
        String profileImage = extra.getString("ProfileImage");
        String email = extra.getString("Email");

        mTvProfileName.setText(name);
        Picasso.get().load(profileImage).into(mCIProfileImage);
        mTvPostNum.setText(postnum);
        mTvFollowerNum.setText(numFollower);
        mTvFollowingNum.setText(numFollowing);
        mTvLinkEmail.setText(email);

        Faker fakerpost = new Faker();
        for (int i = 0;i<30;i++)
        {
            newImage[i]=fakerpost.avatar().image();
        }

        CustomAdapterNewGrid customAdapterNewGrid = new CustomAdapterNewGrid(newImage,this);
        mMyGridView.setAdapter(customAdapterNewGrid);


        mBtFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBtFollow.getText().toString().equals("Follow"))
                {
                    Toast.makeText(NewActivityGridView.this,"Following",Toast.LENGTH_LONG).show();
                    mBtFollow.setText("Following");
                }
                else if(mBtFollow.getText().toString().equals("Following"))
                {
                    Toast.makeText(NewActivityGridView.this,"Unfollowed",Toast.LENGTH_LONG).show();
                    mBtFollow.setText("Follow");
                }
            }
        });

    }
    private void initView()
    {
        mTvPostNum= findViewById(R.id.text_post_num);
        mTvFollowerNum= findViewById(R.id.text_follower_num);
        mTvFollowingNum= findViewById(R.id.text_following_num);
        mTvProfileName= findViewById(R.id.text_profile_name);
        mTvLinkEmail= findViewById(R.id.link_email);
        mCIProfileImage=findViewById(R.id.profile_photo);
        mBtFollow=findViewById(R.id.btn_follow);
    }

}
