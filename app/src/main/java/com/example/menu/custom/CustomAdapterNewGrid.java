package com.example.menu.custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.menu.R;
import com.squareup.picasso.Picasso;

public class CustomAdapterNewGrid extends BaseAdapter {

    String[] image;
    private Context mContext;

    public CustomAdapterNewGrid(String[] image , Context mContext) {
        this.image = image;
        this.mContext=mContext;
    }

    @Override
    public int getCount() {
        return image.length;
    }

    @Override
    public Object getItem(int position) {
        return image[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView= LayoutInflater.from(mContext).inflate(R.layout.subgrid_layour,null);
        ImageView mImage= convertView.findViewById(R.id.new_grid_image);
        Picasso.get().load(image[position]).into(mImage);
        return convertView;
    }
}
