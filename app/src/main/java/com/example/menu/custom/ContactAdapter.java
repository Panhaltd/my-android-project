package com.example.menu.custom;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.menu.R;
import com.example.menu.RecyclerViewActivity;

import java.util.ArrayList;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyContactViewHolder> {

    private ArrayList<Contact> contacts = new ArrayList<>();
    private OnRecyclerViewItemClick listener;

    public ContactAdapter(ArrayList<Contact> contacts, OnRecyclerViewItemClick listener){
        this.contacts = contacts;
        this.listener=listener;
    }

    @NonNull
    @Override
    public MyContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_rec_view,parent,false);
        return new MyContactViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyContactViewHolder holder, int position) {
        holder.ImBigImage.setImageResource(contacts.get(position).getPhoto());
        holder.ImSmallImage.setImageResource(contacts.get(position).getPhoto());
        holder.TvName.setText(contacts.get(position).getName());
        holder.TvPhone.setText(contacts.get(position).getPhone());
        holder.TvWebsite.setText(contacts.get(position).getWebsite());
        holder.TvEmail.setText(contacts.get(position).getEmail());
        holder.TvAddress.setText(contacts.get(position).getAddress());
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    public class MyContactViewHolder extends RecyclerView.ViewHolder{
        ImageView ImBigImage,ImSmallImage;
        TextView TvName,TvPhone,TvWebsite,TvEmail,TvAddress;

        public MyContactViewHolder(@NonNull View itemView) {
            super(itemView);
            ImBigImage = itemView.findViewById(R.id.rec_image);
            ImSmallImage = itemView.findViewById(R.id.small_image);
            TvName = itemView.findViewById(R.id.name_school);
            TvPhone = itemView.findViewById(R.id.number_phone);
            TvWebsite = itemView.findViewById(R.id.website);
            TvEmail = itemView.findViewById(R.id.email);
            TvAddress = itemView.findViewById(R.id.address);

            itemView.setOnClickListener(v -> {
                listener.onItemClick(getAdapterPosition());
            });
        }
    }

}
