package com.example.menu.custom;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.menu.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CustomAdapterGrid extends BaseAdapter {

    private TextView mName;
//    private TextView mAddress;
    private ImageView mProfile;
    private Context mContext;

    private List<UserModel> mUser = new ArrayList<>();

    public CustomAdapterGrid(Context mContext, List<UserModel> mUser) {
        this.mContext = mContext;
        this.mUser = mUser;
    }

    @Override
    public int getCount() {
        return mUser.size();
    }

    @Override
    public Object getItem(int position) {
        return mUser.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView= LayoutInflater.from(mContext).inflate(R.layout.layout_dridview_text,null);
        this.initializeWidgets(convertView);
        this.initializeWidgetsValue(mUser.get(position));
        return convertView;
    }

    private void initializeWidgets(View parent)
    {
        this.mName = parent.findViewById(R.id.tv_gd_name);
//        this.mAddress=parent.findViewById(R.id.tv_gd_address);
        this.mProfile= parent.findViewById(R.id.image_gd_pf);
    }

    private void initializeWidgetsValue(UserModel user)
    {
        this.mName.setText(user.getName());
//        this.mAddress.setText(user.getAddress());
//        this.mProfile.setImageResource(R.drawable.pp3);|
        Picasso.get().load(user.getImage()).into(mProfile);
    }
}
