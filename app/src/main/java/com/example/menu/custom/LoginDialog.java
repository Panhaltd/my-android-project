package com.example.menu.custom;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import com.example.menu.R;

public class LoginDialog extends DialogFragment {

    loginDialogListener listener;

    public interface loginDialogListener{
        void getData(String email,String password,boolean rememberMe);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener=(loginDialogListener) context;
        }catch (ClassCastException ex){
            ex.printStackTrace();
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.login_dialog,null);


//        builder.setTitle("Login Dialog");
        builder.setView(view);



        final EditText mEtEmail = view.findViewById(R.id.editText_email);
        final EditText mEtPass = view.findViewById(R.id.editText_pass);
        final CheckBox mCbRememberMe = view.findViewById(R.id.checkBox);
        final Button mBtLogin = view.findViewById(R.id.btn_dialog_login);

        mBtLogin.setOnClickListener(v -> {
            String email = mEtEmail.getText().toString();
            String pass = mEtPass.getText().toString();
            Boolean isRememberMe = mCbRememberMe.isChecked();

            listener.getData(email,pass,isRememberMe);
            dismiss();

        });


//        builder.setPositiveButton("Login", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//            }
//        });
//        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//            }
//        });

        return builder.create();
    }
}
