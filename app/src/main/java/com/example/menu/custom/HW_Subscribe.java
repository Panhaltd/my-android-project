package com.example.menu.custom;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.menu.R;

public class HW_Subscribe extends DialogFragment {

    SubscribeInterface subscribeListener;

    public interface SubscribeInterface{
        void getData(String email);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            subscribeListener=(HW_Subscribe.SubscribeInterface) context;
        }catch (ClassCastException ex){
            ex.printStackTrace();
        }
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.activity_hw__custom__dialog,null);
        builder.setView(view);

        final EditText mEtEmail=view.findViewById(R.id.editText_Subscribe_email);
        final Button mBtSubmit = view.findViewById(R.id.btn_submit);

        mBtSubmit.setOnClickListener(v -> {
            String youremail = mEtEmail.getText().toString();
            subscribeListener.getData(youremail);
            dismiss();
        });

        return builder.create();
    }
}
