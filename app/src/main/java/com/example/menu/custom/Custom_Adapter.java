package com.example.menu.custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.menu.R;

public class Custom_Adapter extends BaseAdapter {

    private int[] profile;
    private String[] name;
    private String[] job;
    private String[] sex;
    private String[] company;
    private Context context;

    public Custom_Adapter(Context context,int[] profile,String[] name,String[] job,String[] sex,String[] company)
    {
        this.context=context;
        this.profile=profile;
        this.name=name;
        this.sex=sex;
        this.company=company;
        this.job=job;
    }

    @Override
    public int getCount() {
        return name.length;
    }

    @Override
    public Object getItem(int position) {
        return name[position];
    }
    public Object getPhoto(int position){
        return profile[position];
    }
    public Object getJob(int position){
        return job[position];
    }
    public Object getSex(int position){
        return sex[position];
    }
    public Object getCompany(int position){
        return company[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.player_textview,null);
        ImageView imageProfile=convertView.findViewById(R.id.imageView_Player);
        TextView textName = convertView.findViewById(R.id.text_player);
        TextView textJob = convertView.findViewById(R.id.text_job);


        imageProfile.setImageResource(profile[position]);
        textName.setText(name[position]);
        textJob.setText((job[position]));

        return convertView;
    }
}
