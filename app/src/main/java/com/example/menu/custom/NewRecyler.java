package com.example.menu.custom;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.menu.R;

public class NewRecyler extends AppCompatActivity {

    ImageView ImBigImage,ImSmallImage;
    TextView TvName,TvPhone,TvWebsite,TvEmail,TvAddress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_recyler);
        final Bundle extra=getIntent().getExtras();
        Integer image = extra.getInt("kimage");
        String name = extra.getString("kname");
        String number = extra.getString("knumber");
        String website = extra.getString("kwebsite");
        String email = extra.getString("kemail");
        String address = extra.getString("kaddress");

        ImBigImage = findViewById(R.id.rec_image);
        ImSmallImage = findViewById(R.id.small_image);
        TvName = findViewById(R.id.name_school);
        TvPhone = findViewById(R.id.number_phone);
        TvWebsite = findViewById(R.id.website);
        TvEmail = findViewById(R.id.email);
        TvAddress = findViewById(R.id.address);

        ImBigImage.setImageResource(image);
        ImSmallImage.setImageResource(image);
        TvName.setText(name);
        TvPhone.setText(number);
        TvWebsite.setText(website);
        TvEmail.setText(email);
        TvAddress.setText(address);
    }
}
