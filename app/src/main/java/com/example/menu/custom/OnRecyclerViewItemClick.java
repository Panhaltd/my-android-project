package com.example.menu.custom;

public interface OnRecyclerViewItemClick {
    void onItemClick(int position);
}
