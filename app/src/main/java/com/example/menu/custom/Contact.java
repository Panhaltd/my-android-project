package com.example.menu.custom;

public class Contact {

    private Integer photo;
    private String name;
    private String phone;
    private String website;
    private String email;
    private String address;

    public Contact(Integer photo, String name, String phone, String website, String email, String address) {
        this.photo = photo;
        this.name = name;
        this.phone = phone;
        this.website = website;
        this.email = email;
        this.address = address;
    }

    public Integer getPhoto() {
        return photo;
    }

    public void setPhoto(Integer photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
