package com.example.menu.custom;

public class UserModel {
    private String name;
    private String address;
    private String image;
    private String numFollower;
    private String numFollowing;
    private String numPost;
    private String email;


    public UserModel(String name, String image,String numFollower,String numFollowing,String numPost,String email) {
        this.name = name;
//        this.address = address;
        this.image = image;
        this.numFollower=numFollower;
        this.numFollowing=numFollowing;
        this.numPost=numPost;
        this.email=email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumFollower() {
        return numFollower;
    }

    public void setNumFollower(String numFollower) {
        this.numFollower = numFollower;
    }

    public String getNumFollowing() {
        return numFollowing;
    }

    public void setNumFollowing(String numFollowing) {
        this.numFollowing = numFollowing;
    }

    public String getNumPost() {
        return numPost;
    }

    public void setNumPost(String numPost) {
        this.numPost = numPost;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
