package com.example.menu.custom;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.menu.Adapter1;
import com.example.menu.R;

public class Hw_Custom_Adapter extends DialogFragment{

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.custom_dialog_adapter,null);
        builder.setView(view);

        final TextView mTvName= view.findViewById(R.id.tv_name);
        final TextView mTvJob= view.findViewById(R.id.tv_job);
        final ImageView mImage = view.findViewById(R.id.imageView);

//        mTvName.setText(name1.toString());
//        mTvJob.setText(job1.toString());
//        mImage.setImageResource(path);


        return builder.create();
    }


}
