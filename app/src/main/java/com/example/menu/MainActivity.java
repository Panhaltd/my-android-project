package com.example.menu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final TextView tvClickMe = findViewById(R.id.tv_click_me);

        tvClickMe.setOnClickListener(v->{
            PopupMenu popupMenu = new PopupMenu(MainActivity.this,tvClickMe);
            popupMenu.getMenuInflater().inflate(R.menu.popup_menu,popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    int menuId=item.getItemId();
                    switch (menuId)
                    {
                        case R.id.menu_select_all:
                            Toast.makeText(MainActivity.this,"Menu Select All",Toast.LENGTH_LONG).show();
                            break;
                        case R.id.menu_delete:
                            Toast.makeText(MainActivity.this,"Menu Delete",Toast.LENGTH_LONG).show();
                            break;
                    }
                    return false;
                }
            });
            popupMenu.show();
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId=item.getItemId();
        switch (itemId)
        {
            case R.id.menu_add:
                Toast.makeText(MainActivity.this,"Menu Add",Toast.LENGTH_LONG).show();
                break;
            case R.id.menu_select:
                Toast.makeText(MainActivity.this,"Menu Select",Toast.LENGTH_LONG).show();
                break;
            case R.id.menu_edit:
                Toast.makeText(MainActivity.this,"Menu Edit",Toast.LENGTH_LONG).show();
                break;
            case R.id.menu_copy:
                Toast.makeText(MainActivity.this,"Menu Copy",Toast.LENGTH_LONG).show();
                break;
            case R.id.menu_cut:
                Toast.makeText(MainActivity.this,"Menu Cut",Toast.LENGTH_LONG).show();
                break;
            case R.id.menu_paste:
                Toast.makeText(MainActivity.this,"Menu Paste",Toast.LENGTH_LONG).show();
                break;
            case R.id.menu_exit:
                Toast.makeText(MainActivity.this,"Menu Exit",Toast.LENGTH_LONG).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
