package com.example.menu;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Dialog_Act extends AppCompatActivity {

    AlertDialog.Builder builder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_);

        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        final Button mBtShow = findViewById(R.id.btn_show);
        final Button mBtSubject = findViewById(R.id.btn_show_subject);
        builder=new AlertDialog.Builder(Dialog_Act.this);

        mBtSubject.setOnClickListener(v -> {
            String[] subjects = {"Android","IOS","WEB Design","Laravel"};
            AlertDialog.Builder builder = new AlertDialog.Builder(Dialog_Act.this);
            builder.setTitle("Choose Your Subject:");
            builder.setItems(subjects, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Dialog_Act.this,
                            "My subject is"+subjects[which],
                            Toast.LENGTH_LONG).show();
                }
            });
            builder.create().show();
        });


        mBtShow.setOnClickListener(v -> {
            builder.setTitle("Confirmation!")
                    .setMessage("Are you sure to delete?")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Dialog_Act.this,
                            "Yes Deleted",
                            Toast.LENGTH_LONG).show();
                }
            })
                    .setNegativeButton("Cacel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(Dialog_Act.this,
                                    "Operatin is cancelled",
                                    Toast.LENGTH_LONG).show();
                        }
                    }).create().show();
        });
    }
}
