package com.example.menu;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ListViewMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_main);

        String[] a = new String[50];

        final ListView listView=findViewById(R.id.list_view_new);

        for(int i=0;i<50;i++)
        { a[i]="Step-0"+(i+1);
        }

        ArrayAdapter<String> playerAdapter = new ArrayAdapter<>(ListViewMainActivity.this,
                                        R.layout.text_view_list_view,R.id.text_test_list,a);

        listView.setAdapter(playerAdapter);
    }
}
