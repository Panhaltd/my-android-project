package com.example.menu;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.example.menu.custom.HW_Subscribe;

public class HW_Custom_Dialog extends AppCompatActivity implements HW_Subscribe.SubscribeInterface {

    static final String SUBSCRIBE_DIALOG = "Subscribe Dialog";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hw_main_subscribe);

        final Button mBtSubscribe = findViewById(R.id.btn_Subscribe);
        mBtSubscribe.setOnClickListener(v ->{
            HW_Subscribe hw_subscribe = new HW_Subscribe();
            hw_subscribe.show(getSupportFragmentManager(),SUBSCRIBE_DIALOG);
        });
    }

    @Override
    public void getData(String email) {
        Toast.makeText(HW_Custom_Dialog.this,email,Toast.LENGTH_LONG).show();
    }
}
