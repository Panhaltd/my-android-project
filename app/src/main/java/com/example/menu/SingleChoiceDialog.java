package com.example.menu;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class SingleChoiceDialog extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_choice_dialog);
        final Button mBtChooseGender=findViewById(R.id.btn_choose_gender);
        final TextView mTvRole=findViewById(R.id.text_role);

        mBtChooseGender.setOnClickListener(v -> {
            AlertDialog.Builder builder=new AlertDialog.Builder(SingleChoiceDialog.this);
            CharSequence[] genders={"Female","Male","Other"};
            CharSequence[] roles = {"Admin","Editor","Poster","Subscribe"};
            List data = new ArrayList();

            builder.setTitle("Select A Gender")
                    .setIcon(R.drawable.ic_face_black_24dp);

            builder.setMultiChoiceItems(roles, new boolean[]{false, false, false, false}, new DialogInterface.OnMultiChoiceClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                    if (isChecked)
                        data.add(roles[which]);
                    else
                        data.remove((roles[which]));
                    Toast.makeText(SingleChoiceDialog.this,data.toString(),Toast.LENGTH_LONG).show();
                }
            });
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String role = "";
                    for (Object d : data) {
                        role += d.toString() + ",";
                    }
                    role+='\b';
                    mTvRole.setText(role);
                }
            });


//            builder.setSingleChoiceItems(genders, 0, null);
////            builder.setSingleChoiceItems(genders, 0, new DialogInterface.OnClickListener() {
////                @Override
////                public void onClick(DialogInterface dialog, int which) {
////                    Toast.makeText(SingleChoiceDialog.this,genders[which],Toast.LENGTH_LONG).show();
////                    dialog.dismiss();
////                }
////            }
//            builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    int itemCheckedPos = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
//                    Object itemChecked =  ((AlertDialog)dialog).getListView().getItemAtPosition(itemCheckedPos);
//                    Toast.makeText(SingleChoiceDialog.this,"Gender:"+itemChecked.toString(),Toast.LENGTH_LONG).show();
//                }
//            });
//            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                }
//            });
           builder.create().show();
        });
    }
}
