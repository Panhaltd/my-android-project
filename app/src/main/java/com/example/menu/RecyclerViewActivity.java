package com.example.menu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.menu.custom.Contact;
import com.example.menu.custom.ContactAdapter;
import com.example.menu.custom.NewRecyler;
import com.example.menu.custom.OnRecyclerViewItemClick;
import com.example.menu.custom.UserModel;

import java.util.ArrayList;

public class RecyclerViewActivity extends AppCompatActivity implements OnRecyclerViewItemClick {

    private ArrayList<Contact> contacts = new ArrayList<>();
    private RecyclerView recyclerView;
    private ContactAdapter contactAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);

        contacts.add(new Contact(R.drawable.kshrd,"Korean Software HRD","023 456 789","www.kshrd.com.kh","info@kdhrd.edu.kh","Toul Kork, Phnom Penh"));
        contacts.add(new Contact(R.drawable.rupp,"Royal University of Phnom Penh","023 345 666","www.rupp.com.kh","info@rupp.edu.kh","Toul Kork, Phnom Penh"));
        contacts.add(new Contact(R.drawable.paragon,"Paragon International University","023 111 999","www.paragoniu.edu.kh","info@paragoniu.edu.kh","Toul Kork, Phnom Penh"));
        contacts.add(new Contact(R.drawable.norton,"Norton University","023 234 987","www.norton.edu.kh","info@norton.edu.kh","Chroy Chongva, Phnom Penh"));
        contacts.add(new Contact(R.drawable.puc,"Panhasastra University of Cambodia","023 654 654","www.puc.edu.kh","info@puc.edu.kh","Toul Svay Prey, Phnom Penh"));

        recyclerView=findViewById(R.id.my_rec_view);
        contactAdapter=new ContactAdapter(contacts,this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(contactAdapter);

    }

    @Override
    public void onItemClick(int position) {
        Integer simage = contacts.get(position).getPhoto();
        String sname = contacts.get(position).getName();
        String snumber = contacts.get(position).getPhone();
        String swebsite = contacts.get(position).getWebsite();
        String semail = contacts.get(position).getEmail();
        String saddress = contacts.get(position).getAddress();
        Intent intent = new Intent(RecyclerViewActivity.this, NewRecyler.class);
        intent.putExtra("kimage",simage);
        intent.putExtra("kname",sname);
        intent.putExtra("knumber",snumber);
        intent.putExtra("kwebsite",swebsite);
        intent.putExtra("kemail",semail);
        intent.putExtra("kaddress",saddress);
        startActivity(intent);

    }
}
