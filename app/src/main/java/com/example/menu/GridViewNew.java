package com.example.menu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;

import com.example.menu.custom.CustomAdapterGrid;
import com.example.menu.custom.NewActivityAdapter;
import com.example.menu.custom.NewActivityGridView;
import com.example.menu.custom.UserModel;
import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.List;

public class GridViewNew extends AppCompatActivity {

    private List<UserModel> mUser = new ArrayList<>();
    private String[] imagepost=new String[30];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view_new);

        final GridView mMyGridView = findViewById(R.id.gdView);


        Faker faker = new Faker();
        for (int i = 0;i<50;i++)
        {
            String name = faker.name().fullName();
            String numFollower = faker.number().digits(4);
            String numFollowing = faker.number().digits(4);
            String numPost = faker.number().digits(2);
            String profile = faker.avatar().image();
            String email = faker.company().name()+"@gmail.com";
            this.mUser.add(new UserModel(name,profile,numFollower,numFollowing,numPost,email));
        }
//        Faker fakerpost = new Faker();
//        for (int i = 0;i<50;i++)
//        {
//            imagepost[i]=fakerpost.avatar().image();
//        }

        CustomAdapterGrid customAdapterGrid = new CustomAdapterGrid(this,this.mUser);
        mMyGridView.setAdapter(customAdapterGrid);

        mMyGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserModel user = (UserModel) parent.getItemAtPosition(position);
                String name = user.getName();
                String numfollower =user.getNumFollower();
                String numfollowing = user.getNumFollowing();
                String numpost= user.getNumPost();
                String email= user.getEmail();
                String profileimage = user.getImage();

                Intent intent = new Intent(GridViewNew.this, NewActivityGridView.class);
                intent.putExtra("Name",name);
                intent.putExtra("NumPost",numpost);
                intent.putExtra("NumFollower",numfollower);
                intent.putExtra("NumFollowing",numfollowing);
                intent.putExtra("Email",email);
                intent.putExtra("ProfileImage",profileimage);

                startActivity(intent);

            }
        });


    }
}
