package com.example.menu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.example.menu.custom.LoginDialog;

public class Dialog_Activity extends AppCompatActivity implements LoginDialog.loginDialogListener{

    static final String LOGIN_DIALOG = "Login Dialog";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_2);
        final Button mBtLogin = findViewById(R.id.btn_login_dialog);

        mBtLogin.setOnClickListener(v -> {
            LoginDialog loginDialog = new LoginDialog();
            loginDialog.show(getSupportFragmentManager(),LOGIN_DIALOG);
        });
    }

    @Override
    public void getData(String email, String password, boolean rememberMe) {
        Toast.makeText(Dialog_Activity.this,email+"-"+password+"-"+rememberMe,Toast.LENGTH_LONG).show();
    }
}
